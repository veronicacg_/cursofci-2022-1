import numpy as np
import matplotlib.pyplot as plt

from sistema import particula #Importamos la clase

m = 9.11e-31 #Definimos las variables en el sistema internacional.
q = -1.602e-19 
Ek = 18.6*abs(q)
theta = np.radians(30) #Angulo en radianes.
B = 600e-6 #T

p=particula(Ek,m,q,theta,B) #Redefinimos y llamamos la clase para la parìcula con los datos dados.
V=p.v()
Vz = V*np.sin(theta) #Velocidad en z.

dt = 0.01
t = np.arange(0,10000*dt+dt,dt) #Arreglo de t para graficar.
R = (p.m*V*np.cos(theta))/(p.q*B)#Radio de larmor.
def S(t,R,V): #Definimos una funcion parametrizada para t.
    return np.array([R*np.cos(t),R*np.sin(t),V*t]) 

x,y,z = S(t,R,Vz)

fig=plt.figure("Trayectoria")
ax=(fig.gca(projection="3d")) #Graficamos en 3D.

ax.plot(x,y,z,'r.')
plt.show()