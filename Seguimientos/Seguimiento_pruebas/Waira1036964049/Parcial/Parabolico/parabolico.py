import numpy as np
import matplotlib.pyplot as plt

class parabolico:

    def __init__(self,hi,v0,theta,t):
        self.hi=hi
        self.v0=v0
        self.theta=theta*(np.pi/180)
        self.t=np.arange(0,40,0.1)


    def vx(self):
        vx=self.v0*np.cos(self.theta)
        return vx
    
    def vy(self):
        vy=self.v0*(np.sin(self.theta))
        return vy
    
    def x(self):
        return self.vx()*self.t


    def trayectoria(self):
        plt.plot(self.t,self.x())
        plt.xlabel("t")
        plt.ylabel("x")
        plt.title("holis")
        plt.show()





