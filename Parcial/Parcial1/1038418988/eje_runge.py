from RungeKutta import PenduloNoLineal


print("INGRESE LOS SIGUIENTES VALORES:")
l=float(input("Longitud del pendulo (m):"))
h=float(input("Tamaño de paso:"))
a=int(input("Limite inferior, a:"))
b=int(input("Limite superior, b:"))

r=PenduloNoLineal(l,h)
print("Las soluciones a la ecuacion diferencial son:{}".format(r.RungeKutta(a,b)))

r.DesplazamientoAngular()