import numpy as np
import parab as pb
import matplotlib.pyplot as plt

if __name__=="__main__":
    '''
    Metodos de la clase yMov que tambien hereda la clase xMov:
    Vel: Calcula la velocidad en el respectivo eje
    tVuelo: Calcula el tiempo de vuelo
    Pos: Calcula las posiciones desde la posicion inicial hasta la posicion final
    maxPos: Calcula el alcance maximo
    '''

    x0, y0, ang, v0, ax, ay = 0, 10, 30, 20, 0.1, 9.8 #Parametros iniciales
    t = 10 #tiempo donde calcula la velocidad
    movY = pb.xMov(y0, ang, v0, ay, t)

    movX = pb.xMov(x0, ang, v0, ax, t)

    plt.plot(movX.Pos(), movY.Pos(), color = 'darkcyan')
    plt.title("Trayectoria en xy")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig('Trayectoria en xy.png')
    #movX.Trayectory()
    
    