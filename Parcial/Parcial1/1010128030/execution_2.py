from ED import *
import numpy as np

if __name__ == '__main__':
    
    #Definición de función a la que es igual la segunda derivada
    def function(t,v,u):
        g = 9.8
        l = 10
        return - (g/l)*np.sin(u)
    
    #Se instancia un objeto con las condiciones iniciales estipuladas y se grafica
    Func = PenduloNoLineal(u0 = 0.1, t0 = 0, v0 = 0, paso = 0.001, funcion = function, rango = 20)
    Func.DesplazamientoAngular()
