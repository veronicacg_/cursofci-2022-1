import pandas as pd
import matplotlib.pyplot as plt

#1)JOIN DATAFRAME

## Se leen datos. diccionario 

## a)

data_1 = {
        'id': ['1', '2', '3', '4', '5'],
        'primer_nombre': ['Alex', 'Amy', 'Valentina', 'Alice', 'Lina'], 
        'apellido': ['Anderson', 'Ruales', 'Marin', 'Barbosa', 'Robles']}

df1= pd.DataFrame(data_1)
df1.head()


data_2 = {
        'id': ['4', '5', '6', '7', '8'],
        'primer_nombre': ['Luis', 'Brian', 'Mariana', 'Marcela', 'Carlos'], 
        'apellido': ['Ruiz', 'Giralgo', 'Ortegon', 'Palacios', 'Gallego']}

df2= pd.DataFrame(data_2)
df2.head()

data_3 = {
        'id': ['1', '2', '3', '4', '5', '7', '8', '9', '10', '11'],
        'test_id': [51, 15, 15, 61, 16, 14, 15, 1, 61, 16]}

df3= pd.DataFrame(data_3)
df3.head()


## Númeral concatena. Unión por columnas

df4 = df1.join(df2,  lsuffix= "_left")  # une a la derecha df1 el data frame 2 (df2.)

df4

## Unión por filas.

uf = pd.concat([df1, df2], axis=0, ignore_index=True) ## uniorn por filas.
uf

# Hacer merge para todos los datos y el tercer df con valor id:

df6 = pd.merge(uf,df3)
df6



# hacer merge con datos que tienen el mismo 'id'

df8 = pd.merge(df1,df2, left_on='id', right_on='id')  ## agrupa (merge) datos que tienen el mismo id.
df8

## GROUPBY

alco = pd.read_csv('datos_alcohol.txt')

alco  ## dataframe.

## selecciono las columnas de interes a trabajar.
## columnas de " pais" , " porciones_cervez " y "continente"

alco.columns

## ¿ Que continente toma mas cerveza?
# selecciono columnas de interés.

alcodf =alco[["porciones_cerveza",'continente']]
alcodf.head()

# selecciona por continentes y promedia porciones de cerveza.
gconti = alcodf.groupby("continente").mean()
gconti.head()

# ordena de mayor a menor:

gconti.sort_values('porciones_cerveza',ascending= False).head()

# El continente que mas consumen cerveza es EUROPA, con 193 porciones de cerveza en promedio.

## Análisis Vino:

vinos = alco[["porciones_vino"]]
vinos.head()

vinos.describe() ## realiza análisis estadístico del consumo de vino.

'''En promedio se toma 49 porciones de vino en todos los paises de el
 mundo con una altísima incertidumbre ya que que la desviación estandar 
 es 79 , esto debido a que hay paises en los que se toma mucho vino, pero
  por el contrario en otros se bebe muy poco. teniendo en un máximo de 370
   y un mínimo de 59 porciones de vino.'''

## agrupar consumo para cada continente medio, mínimo y máximo.

## valor medio.
vmedio = alco.groupby("continente").porciones_licor.mean()
vmedio

## valor minimo.
vmin = alco.groupby("continente").porciones_licor.min()
vmin

## valor máximo

vmax= alco.groupby("continente").porciones_licor.max()
vmax

## crea data frame que contiene cada continente con valor medio,min y maximo.

tablalicor = pd.concat([vmedio,vmin, vmax], axis=1, ignore_index=True) ## uniorn por filas.
tablalicor.columns =['Valor_medio','Valor_min','Valor_max']
tablalicor

## Ahora imprimamos consumo medio de alcohol para todas las columnas.

prom_alco= alco.groupby("continente").mean() ## imprime consumo medio de alcohol para todas las columnas.
prom_alco


### VISUALIZACIÓN

vis = pd.read_csv('visualizacion.txt')
vis

# Selecciona columna.

tbill = vis['total_bill']
tbill

# Grafica histograma.

plt.hist(tbill)
plt.grid()

# Crea scatter plot, relacionando columnas total_bil y tip.

tip = vis['tip']

plt.scatter(tip,tbill)
plt.xlabel("tip")
plt.ylabel("total_bill")
plt.grid()

## relacion entre days y total_bill

ratio = vis[['day','total_bill']] ## contiene days

ratio

relation = ratio.groupby("day").mean() 
relation

#Relacionando los dias de la semana con su respectivo total bill en promedio. 
#Asi en promedio los dias domingos hay mas total_bill .


## BOX PLOT:

vis.boxplot( by = 'time' , column= ['total_bill'])












